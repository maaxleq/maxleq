global.dirs["main"] = __dirname

const express = require("express")
const path = require("path")
const renderController = require(path.join(__dirname, "controllers", "render_controller"))

const router = express.Router()

router.use(express.static(path.join(__dirname, "public")))
router.use(renderController)

module.exports = router