function getQualities(){
    let qualities = []
    $("#qualities").children().each((index, elem) => {
        qualities.push($(elem).attr("data-text"))
    })

    return qualities
}

function typeQualitiesWord(word, delay=100, last=false){
    let letters = word.split("")
    let line = $("#qualities").children("[data-text=\"" + word + "\"]")
    line.text("")
    $(line).addClass("has-cursor")
    typeQualitiesLetter(line, letters, delay, last)
}

function typeQualitiesLetter(el, letters, delay=100, last=false){
    if (letters.length > 0){
        let letter = letters.shift()
        $(el).text(el.text() + letter)

        setTimeout(() => {
            typeQualitiesLetter(el, letters, delay, last)
        }, delay)
    }
    else if (! last){
        $(el).removeClass("has-cursor")
    }
}

function typeQualities(qualities, delay=100){
    if (qualities.length > 0){
        let quality = qualities.shift()
        let wordDelay = delay * quality.length
        typeQualitiesWord(quality, delay, qualities.length == 0)
        setTimeout(() => {
            typeQualities(qualities, delay)
        }, wordDelay)
    }
}

$(document).ready(() => {
    typeQualities(getQualities(), 150)
})
