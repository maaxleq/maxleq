function toggleLangDropdown(){
    if ($("#lang-dropdown").hasClass("hidden")){
        $("#lang-dropdown").removeClass("hidden")
    }
    else {
        $("#lang-dropdown").addClass("hidden")
    }
}

$(document).ready(() => {
    $(document).click((event) => {
        if ($(event.target).closest("#lang-toggle").length == 0 && $(event.target).closest("#lang-button").length == 0){
            $("#lang-dropdown").addClass("hidden")
        }
    })

    $("#lang-button").click(toggleLangDropdown)

    $("#lang-dropdown p").click((event) => {
        let lang = $(event.target).attr("data-lang")
        let url = window.location.href.split("?")[0]

        window.location.href = url + "?lang=" + lang
    })

    $(document).scroll(() => {
        if (window.scrollY > 0){
            $("header").addClass("header-back")
        }
        else {
            $("header").removeClass("header-back")
        }
    })
})