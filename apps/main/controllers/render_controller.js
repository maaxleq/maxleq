const express = require("express")
const {TwingEnvironment, TwingLoaderFilesystem} = require("twing");
const path = require("path")
const varImporter = require("variable-importer")
const acceptLangParser = require("accept-language-parser")

const loader = new TwingLoaderFilesystem(path.join(global.dirs.main, "templates"));
const twing = new TwingEnvironment(loader);
const importer = varImporter(path.join(global.dirs.main, "lang"), {
    format: "yml"
})
const router = express.Router()

const text = {}
text.fr = importer.import("fr")
text.en = importer.import("en")

function getLang(req){
    return req.query.lang || acceptLangParser.parse(req.headers["accept-language"])[0].code || "fr"
}

router.get("/", (req, res) => {
    let lang = getLang(req)

    twing.render("main.twig", {
        lang : lang,
        text: text[lang],
        page: "home"
    }).then((output) => {
        res.send(output)
    })
})

router.get("/contact", (req, res) => {
    let lang = getLang(req)

    twing.render("contact.twig", {
        lang : lang,
        text: text[lang],
        page: "contact"
    }).then((output) => {
        res.send(output)
    })
})

module.exports = router