// Set the current directory as a reusable working directory
global.dirs = {}
global.dirs["root"] = __dirname

// Configure environment
require("dotenv").config()

// Import modules
const fs = require("fs")
const express = require("express")
const http = require("http")
const https = require("https")
const path = require("path")
const subdomainApps = require("subdomain-apps")

// Create the root app
const app = express()

// Process command line args
const args = process.argv.slice(2)

// Check is https is wanted
const isHttps = args.includes("-s")

if (isHttps){
    app.use ((req, res, next) => {
        if (req.secure) {
            next()
        } else {
            res.redirect('https://' + req.headers.host + req.url)
        }
    })
}

/*app.use(subdomainApps(process.env.DOMAIN,
    path.join(__dirname, "subdomain_apps.json"),
    path.join(__dirname, "apps")))*/

app.use(require("./apps/main/index"))

// Create server and listen
if (isHttps){

    const privateKey  = fs.readFileSync(process.env.PRIVATE_KEY_PATH, "utf8")
    const certificate = fs.readFileSync(process.env.CERTIFICATE_PATH, "utf8")

    const credentials = {key: privateKey, cert: certificate}

    https.createServer(credentials, app).listen(process.env.HTTPS_PORT)
}

http.createServer(app).listen(process.env.HTTP_PORT)
